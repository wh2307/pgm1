#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "mylist.h"
#include "struct.h"

// remove a component from the linked list of components. Iterate through the list, once it's found, we remove it (by manipulating the nodes).
// we then return this node.

//ISSUE WHEN THE NODE TO REMOVE IS THE LAST NODE!
struct Node *remove_component(struct List *list, struct ll_component *file) {
	struct Node *node = list->head;
	struct Node *temp = NULL;
	struct Node *back;
	
	while (node) {
		struct ll_component *component = (struct ll_component *)node->data;
		if (strcmp(component->filename, file->filename) == 0) {
			if (node->next != 0) {
				temp = node->next;
				node->data = temp->data;
				node->next = temp->next;
			}
			else {
				temp = node;
				back->next = node->next;
			}
		}
		
		back = node;
		node = node->next;
	}
	
	return temp;
}

// search a component's siblings for a component.
struct ll_component *find_component(struct List *list, char *filename) {
	struct Node *node = list->head;
	
	while (node) {
		struct ll_component *return_val = (struct ll_component *)node->data;
		if (strcmp(return_val->filename, filename) == 0) {
			return return_val;
		}
		
		node = node->next;
	}
	
	return NULL;
}

// create a new sibling component and add it to the linked list.
struct ll_component *init_component(struct List *list, char *file) {
    struct ll_component *tmp = (struct ll_component *) malloc(sizeof(struct ll_component));

    tmp->filename = strdup(file);
    initList(&tmp->next);
    initList(&tmp->acl);
		
    addTail(list, tmp);
	
	return tmp;
}

// copies the ACL of parent to the ACL of child.
int copy_acl(struct ll_component *parent, struct ll_component *child) {
	struct List *parent_acl = &parent->acl;
	struct Node *node = parent_acl->head;
	
	while (node) {
		struct acl *parent_entry = (struct acl *)node->data;
		
		init_acl(child, parent_entry->user, parent_entry->group, parent_entry->priv);
		
		node = node->next;
	}
	
	return 0;
}

// create a new ACL entry and add it to the component's ACL.
struct Node *init_acl(struct ll_component *component, char *user, char *group, char *perm) {
	struct Node *node;
	
	struct acl *existing_acl = find_user_acl(&component->acl, user);
	
	if (existing_acl != NULL) {
		if (strcmp(group, existing_acl->group) == 0) {
			return NULL;
		}
	}
	
	struct acl *entry = (struct acl *) malloc(sizeof(struct acl));
	
    entry->user = strdup(user);
    entry->group = strdup(group);
    entry->priv = strdup(perm);
	entry->component = component;
	
	node = addTail(&component->acl, entry);
	
	return node;
}

// Loops through all ACL entries for a given ACL and destroy them.
void destroy_acl_entries(struct List *acls) {
	while (acls->head != 0) {
		struct Node *node = acls->head;
		struct acl *entry = (struct acl *) node->data;
		acls->head = acls->head->next;
		
		free(entry->user);
		free(entry->group);
		free(entry->priv);
		free(entry);
		free(node);
	}
}

// Destroy our file system by looping through all lists, destroying the ACLs, and freeing the component after.
void destroy_file_system(struct List *fs) {
	while (fs->head != 0) {
		struct Node *node = fs->head;
		struct ll_component *directory = (struct ll_component *) node->data;
		destroy_file_system(&directory->next);
		destroy_acl_entries(&directory->acl);
		
		fs->head = fs->head->next;
		
		free(directory->filename);
		free(directory);
		free(node);
	}
}

// Traverse the file system and invoke f on each component.
void traverse_fs(struct List *fs, void (*f)(void *)) {
	struct Node *node = fs->head;
    
	while (node) {
		f(node->data);
		
		struct ll_component *directory = (struct ll_component *) node->data;
		traverse_fs(&directory->next, f);
		
		node = node->next;
    }
}

// searches the file system for a given user and returns the final component along a path.
// used during the user definition stage only.
struct ll_component *findUser(struct List *fs, char *user) {
	int com_found = 0;
	struct ll_component *directory;
	struct ll_component *return_value = NULL;
	struct Node *node = fs->head;
	
	while (node) {
		directory = (struct ll_component *) node->data;
		struct Node *acl = (&directory->acl)->head;
		while (acl) {
			struct acl *entry = (struct acl *) acl->data;
			if (strcmp(entry->user, user) == 0) { // user is found!
				return_value = directory;
				break;
			}
			acl = acl->next;
		}
		
		if (return_value == NULL) {
			return_value = findUser(&directory->next, user);
		}
		else {
			break;
		}
		
		node = node->next;
	}
	return return_value;
}

// given an ACL, search all entries for a given user.
struct acl *find_user_acl(struct List *acls, char *user) {
	struct acl *return_val;
	struct Node *node = acls->head;
	
	while (node) {
		return_val = (struct acl *)node->data;
		
		if (strcmp(return_val->user, user) == 0)
			return return_val;
		
		node = node->next;
	}
	
	return NULL;
}

/*
returns the integer for a given permission, for easy comparison.
0 - no permission
1 - read-only
2 - write-only
3 - read-write
-1 - error
*/
static int return_permission(char *perm) {
	if (perm == NULL)
		goto error;
	
	if (strcmp(perm, "rw") == 0)
		return 3;
	
	if (strcmp(perm, "w") == 0)
		return 2;
	
	if (strcmp(perm, "r") == 0)
		return 1;
	
error:
	return -1;
}

// given an ACL, evaluate it for a given user and group. If nothing matches, return 0, which means no permission.
int evaluate_acl(struct List *acls, char *user, char *group) {
	struct Node *node = acls->head;
	
	while (node) {
		struct acl *entry = (struct acl *)node->data;
		
		if ((strcmp(entry->user, user) == 0) // match users
			|| ((strcmp(entry->user, "*") == 0) && (strcmp(entry->group, group) == 0)) // match group
			|| ((strcmp(entry->user, "*") == 0) && (strcmp(entry->group, "*") == 0))) // match world
			return return_permission(entry->priv);
		node = node->next;
	}
	
	return 0;
}