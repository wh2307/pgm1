Name: William Hom
UNI : wh2307
Assignment 1

Usage
-------------------------------------------------------------------------------
Usage is simple. This program takes no arguments. You can run the program with 
"./acl_simulation". This will require the user to write the inputs in one line
at a time; therefore, this method is not recommended.

You may also pipe a file in using cat. A good example would be for you to type 
"cat test_data.text | ./acl_simulation". This will read the input from your 
file and process them.

Of course, as the assignment requires, you can also use make exec. However, you
must specify the file as an argument. See the below for an example:

make exec ARG=test_data.txt

The above will execute the program with test_data.txt as the input file.

On lines 650 and 651 in test.c, I included some lines meant for debugging
purposes. This will traverse the file system and print out the component names
as well as the ACL associated with it. The ordering is a little tricky (it goes
directory, then subdirectory, then sub-subdirectory, etc.) but it should be
helpful in checking the output. The submission has these two lined commented.

Implementation details
-------------------------------------------------------------------------------
Data structures - The data structure of the filesystem is made up of a
component struct consisting of a file name, a list of ACL entries, and two 
linked lists. One represents "sibling" directories to the current component,
while the other represents "child" directories. In this way, we can imagine the
file system as a 2-dimensional linked list where vertical nodes are siblings
and horizontal nodes are children.

NOTE: The linked list implementation is used from a previous class taken at
Columbia Unversity - Advanced Programming (COMSW3157) taught by Jae Woo Lee
with some slight modifications.

The traversal of the linked list given a filename is to first tokenize the 
filename by "/", then searching the file system vertically before horizontally. 
If the given token can't be found at the current level, an error is thrown.

If a given input line provided during both the user definition and operations
exceeds 4096 bytes, it will be considered invalid. This is by design. As the
parameters of the assignment say that a filename can be 256 bytes long, that
leaves 3840 bytes for the user and group. Any attempts to allocate more than
that amount will be identified as "malicious" behavior and not be processed.

There is an implicit "/" directory that cannot be written to. Thus, after the
user definition stage, one cannot create or delete top-level components. Both
CREATE and DELETE check for write permissions of the previous component. If
one tries to create or delete a top-level component, there will be none to
check against and an error will be thrown.

There is no /home directory defined at the beginning of the program; /tmp is
defined with world read/write permissions. This is to accommodate the fact that
not every test data will actually use /home as its "base". The /tmp component
is created after the user definition stage; this is done so that we can specify
a user "*" (which would not be allowed otherwise, given that only one user can
have one file).

It was noticed that test data written in a Windows environment (via Notepad++)
will completely throw this program off. This is due to the different ways in
which Windows and Linux treat newlines. Windows adds a carriage return "\r" in
addition to "\n", whereas Linux only reads "\n". I couldn't code around this
easily.

For the ACL command, it is assumed that there will be a "." that follows at
some point. Therefore, if one uses this command and does not follow up with one
the program will assume the entire rest of the input are ACL entries and try to
process them accordingly. Similarly, if after the ACL command an entry is 
deemed invalid, then the ACL of the component will NOT be modified. The ACL 
command takes an all-or-nothing approach.

Given the functionality of fgetc(), after the second while loop()
processes the last line, we need to check if there's indeed something still
left to process. This is because when the file reaches EOF, the loop breaks,
so we check the buffer one last time to ensure that everything is processed
correctly. Note, however, that if there is a blank last line in your input file
you will get an additional line with "Invalid Command Entered".

For the operations:

1. The WRITE command is dependent on the READ command.
2. The ACL command is dependent on the WRITE command.
3. DELETE and CREATE relies on both the READ and WRITE commands. It needs the
former to check for the existence of the file, then usees the latter on a
portion of the string (up to the previous component) to determine whether 
write permissions will allow the user to run the command.
4. The CREATE command does not care whether or not it is followed by the ACL
command. It will automatically copy the previous component's ACL. That way,
whether the next command is an ACL or not doesn't matter. It is up to the user
to ensure that the proper ACL is specified after creating a new file.

The use of flags on READ and WRITE serve to suppress messages so that we don't
see two for the same command. Though one can easily have the second line say
"WRITE failed because READ failed - see above", this can lead to some rather
unsightly outputs.
