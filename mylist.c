#include "mylist.h"
#include <stdlib.h>
#include <stdio.h>

// Creates a new node with the data, sets its next to the list's head. Then assigns the list's head to the new node.
struct Node *addFront(struct List *list, void *data) {
	struct Node *newNode = malloc(sizeof(struct Node));
	newNode->data = data;
	newNode->next = list->head;
	list->head = newNode;	

	return newNode;
}

struct Node *addTail(struct List *list, void *data) {
	struct Node *newNode;
	reverseList(list);
	newNode = addFront(list, data);
	reverseList(list);

	return newNode;
}

// Traverses the list using a while loop. After each loop, set the head to be the next node. Note we use a reference node so we always know where the list head is. We assign the list's head to this reference node at the end.
void traverseList(struct List *list, void (*f)(void *)) {
    struct Node *node = list->head;
    while (node) {
		f(node->data);
		node = node->next;
    }
}

// Searches the node by traversing the list and comparing with dataSought using the provided function pointer. Note again the use of a reference node to keep track of the list head.
struct Node *findNode(struct List *list, const void *dataSought, int (*compar)(const void *, const void *)) {
	struct Node *refNode = list->head;
	
	while (list->head != 0) {
		if (compar(list->head->data, dataSought) == 0) {
			struct Node *retNode = list->head;
			list->head = refNode;
			return retNode;
		}
		list->head = list->head->next;
	}

	list->head = refNode;
	return NULL;	
}

// Multiplies a double by -1.
void flipSignDouble(void *data) {			
	*(double *)data = *(double *)data * -1.;
}

// Compares two doubles; returns 0 for true, 1 for false.
int compareDouble(const void *data1, const void *data2) {
	if (*(double *)data1 == *(double *)data2) {
		return 0;
	}

	return 1;
}

// Removes the first node in the list. Creates a node from the list's head, then assigns the data to retData. The list head is set to be the list head's next node. We then deallocate the node, returning retData.
void *popFront(struct List *list) {
	if (isEmptyList(list) == 1) {
		return NULL;
	}

	struct Node *delNode = list->head;
	void *retData = delNode->data;
	list->head = list->head->next;
	free(delNode);

	return retData;
}

// Traverse through the node. In each loop, assign the list's head to data, then assign the list head to be the list head's next node. Then free data.
void removeAllNodes(struct List *list) {
	while (list->head != 0) {
		struct Node *data = list->head;
		list->head = list->head->next;
		free(data);
	}
}

// If the previous node is NULL, then simply call addFront(). Otherwise, allocate memory for a new node element. Assign the data to it, then set its next node to be prevNode's next node. Finally, prevNode's next node will be element.
struct Node *addAfter(struct List *list, struct Node *prevNode, void *data) {
	if (prevNode == NULL) {
		return addFront(list, data);
	}

	struct Node *element = malloc(sizeof(struct Node));
	if (element == NULL) {
		return NULL;
	}
	
	element->data = data;
	element->next = prevNode->next;
	prevNode->next = element;

	return element;
}

// Reverses the linked list. In each loop, set the next node to be the current node's next node. The current node's next node is the previous node. The previous node is assigned to the current node and the current node is assigned to the next node. Once this is done, have the list's head point to prevN.
void reverseList(struct List *list) {
	struct Node *prevN = NULL;
	struct Node *currN = list->head;
	struct Node *nextN;

	while (currN != 0) {
		nextN = currN->next;
		currN->next = prevN;
		prevN = currN;
		currN = nextN;
	}

	list->head = prevN;
}
