#ifndef _STRUCT_H_
#define _STRUCT_H_

struct ll_component {
    char *filename;
    struct List acl;
    struct List next; //next represents the subdirectories of a current ll_component. Its data will a ll_component.
};

struct acl {
    char *user;
    char *group;
    char *priv;
	struct ll_component *component;
};

void traverse_fs(struct List *fs, void (*f)(void *));
struct ll_component *init_component(struct List *list, char *file);
struct Node *init_acl(struct ll_component *component, char *user, char *group, char *perm);
void destroy_file_system(struct List *fs);
void destroy_acl_entries(struct List *acls);
struct ll_component *findUser(struct List *fs, char *user);
struct ll_component *find_component(struct List *list, char *filenamea);
struct acl *find_user_acl(struct List *acls, char *user);
int copy_acl(struct ll_component *parent, struct ll_component *child);
int evaluate_acl(struct List *acls, char *user, char *group);
struct Node *remove_component(struct List *list, struct ll_component *file);

#endif
