#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "mylist.h"
#include "struct.h"

#define MAXBUFFER 4096
#define MAXFILENAME 256
#define MAXCOMPONENT 16

static int linenumber = 0;
struct ll_component *verify;
struct acl *verify_acl;

static void die(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1); 
}

static void printACL(void *p) {
	struct acl *x = (struct acl *) p;
	printf("%s %s %s\n", x->user, x->group, x->priv);
}

static void printComponent(void *p) {
	struct ll_component *x = (struct ll_component *) p;
	printf("->%s \n", x->filename);
	traverseList(&x->acl, &printACL);
}

// adds world-readability to a given component.
static void addWorldRead(void *p) {
	struct ll_component *x = (struct ll_component *) p;
	init_acl(x, "*", "*", "r");
}

// validate the string during user definition stage. String length must be greater than 0, it can only be lower case letters (except *).
static int validate_string(char *string) {
	int i;
	
	if (strlen(string) == 0)
		return -1;
	
	for (i = 0; i < strlen(string); i++) {
		if (!isalpha(string[i])) {
			if (strlen(string) != 1 && strstr(string, "*") == NULL)
				return -1;
		}
		else if (isupper(string[i]))
			return -1;
	}

	return 0;
}

static int validate_component(char *string) {
	int i;
	
	if (strlen(string) == 0)
		return -1;
	
	for (i = 0; i < strlen(string); i++) {
		if (!isalpha(string[i])) {
			if (strstr(string, ".") == NULL)
				return -1;
		}
	}

	return 0;
}

// returns the filename of the previous component. Needed when creating and deleting files.
static char *find_last_path(char *input) {
	char *return_value = (char *) calloc(strlen(input),sizeof(char));
	
	char *token = strtok(input, "/");
	
	while (token != NULL) {
		char *component = strdup(token);
		
		if ((token = strtok(NULL, "/")) == NULL) {
			free(component);
			break;
		}
		
		strcat(strcat(return_value, "/"), component);
		free(component);
	}
	
	return return_value;
}

// validate user definition input. first it validate the string, then whether or not the user exists. then look for the //
// but then make sure there's also a filename. the first character in a filename must be "/". filename's max size is 256.
static int validate_ud_input(struct List *fs, char *user, char *group, char *filename) {
	if (validate_string(user) < 0 || validate_string(group) < 0) {
		fprintf(stdout, "%d \t X \t username and group cannot be empty and must contain lower case letters only\n", linenumber);
		return -1;
	}
	
	verify = findUser(fs, user);
	if (verify != NULL) { // user already exists
		if (strlen(filename) == 0) { // no filename is provided			
			verify_acl = find_user_acl(&verify->acl, user);
			if (strcmp(verify_acl->group, group) == 0) { // user exists, check if user is already in that group
				fprintf(stdout, "%d \t X \t username and group already exists\n", linenumber);
				return -1;
			}
			else { // user already exists, no filename provided, not part of the same group, so add an ACL entry
				return 0;
			}
		}
		else { // only one file per user allowed in the definition stage
			fprintf(stdout, "%d \t X \t only one file per user allowed\n", linenumber);
			return -1;
		}
	}
	else if (strlen(filename) == 0) { // user doesn't exist; we need a file.
		fprintf(stdout, "%d \t X \t invalid filename input (no filename provided)\n", linenumber);
		return -1;
	}
	
	if (strstr(filename, "//") != NULL || filename[0] != '/') { // per instructions
		fprintf(stdout, "%d \t X \t invalid filename input (check your /'s)\n", linenumber);
		return -1;
	}
	if (strlen(filename) > MAXFILENAME) { // filename must be less than 256 bytes per instructions
		fprintf(stdout, "%d \t X \t filename input is too long (limit 256)\n", linenumber);
		return -1;
	}
	
	return 1;
}
/*
// validate input from operations stage. make sure it's a valid command first. then validate the input.
1 - permitted
0 - error
-1 - error
*/
static int validate_ops_input(char *command, char *user, char *group, char *filename) {
	if ((strcmp(command, "CREATE") != 0) && (strcmp(command, "READ") != 0) && (strcmp(command, "WRITE") != 0)
	&& (strcmp(command, "DELETE") != 0) && (strcmp(command, "ACL") != 0)) {
		fprintf(stdout, "%d \t X \t invalid command entered\n", linenumber);
		return -1;
	}
	
	if (validate_string(user) < 0 || validate_string(group) < 0) { // validate the user strings
		fprintf(stdout, "%d \t X \t %s \t username and group must contain lower case letters only\n", linenumber, filename);
		return -1;
	}
	
	if (strstr(filename, "//") != NULL || filename[0] != '/') { // validate filename
		fprintf(stdout, "%d \t X \t invalid filename input (check your /'s)\n", linenumber);
		return -1;
	}	
	
	if (strlen(filename) > MAXFILENAME) {
		fprintf(stdout, "%d \t X \t filename input is too long (limit 256)\n", linenumber);
		return -1;
	}
	
	return 1;
}

// executes the read command. override argument is used by other functions (to control message printing).
static struct ll_component *run_read(struct List *fs, char *user, char *group, char *filename, int override) {
	struct ll_component *return_value = NULL;
	struct ll_component *directory;
	char *dupfilename = strdup(filename);
	char *token = strtok(dupfilename, "/");
	
	if ((directory = find_component(fs, token)) == NULL) { // find the top-level directory
		if (override < 0) {
			printf("%d \t X \t %s \t READ operation failed - file not found\n", linenumber, filename);
		}
		goto exit;
	}
	
	while ((token = strtok(NULL, "/")) != NULL) { // check each subdirectory along the path.
		if (directory != NULL) {
			int perm = evaluate_acl(&directory->acl, user, group);
			if (perm != 1 && perm != 3) {
				if (override < 0) {
					printf("%d \t N \t %s \t READ operation failed - %s.%s has no permission\n", linenumber, filename, user, group);
					goto exit;
				}
			}
			else
				directory = find_component(&directory->next, token);
		}
		else 
			break;
	}
	
	if (directory == NULL) { // the last component wasn't found
		if (override < 0)
			printf("%d \t X \t %s \t READ operation failed - file not found\n", linenumber, filename);
	}
	else {
		int perm = evaluate_acl(&directory->acl, user, group);

		if (perm != 1 && perm != 3) {
			if (override == 1)
				return_value = directory;
			else if (override < 0)
				printf("%d \t N \t %s \t READ operation failed - %s.%s has no permission\n", linenumber, filename, user, group);
			
			goto exit;
		}
		else {
			if (override < 0)
				printf("%d \t Y \t %s \t READ operation was successful\n", linenumber, filename);
			
			return_value = directory;
			goto exit;
		}
	}
	
exit:
	free(dupfilename);
	return return_value;
}

// execute the write command. first run read, but override it so we don't print messages and also ensure that we get the component.
// then evaluate the last component to see if we can write to it.
static struct ll_component *run_write(struct List *fs, char *user, char *group, char *filename, int override) {
	struct ll_component *last_directory = run_read(fs, user, group, filename, 1);
	
	if (last_directory == NULL) {
		if (override == 0)
			printf("%d \t N \t %s \t WRITE operation failed because READ operation was unsuccessful\n", linenumber, filename);
		return NULL;
	}
	
	int perm = evaluate_acl(&last_directory->acl, user, group);
	
	if (perm != 2 && perm != 3) {
		if (override == 0)
			printf("%d \t N \t %s \t WRITE operation failed - %s.%s has no permission\n", linenumber, filename, user, group);
		return NULL;
	}
	
	if (override == 0)
		printf("%d \t Y \t %s \t WRITE operation was successful\n", linenumber, filename);
	
	return last_directory;
}

// delete a component. first run a read through it to see if the file can be accessed and found.
static int run_delete(struct List *fs, char *user, char *group, char *filename) {
	// check that file exists and can be accessed.
	struct ll_component *directory = run_read(fs, user, group, filename, 1);
	
	if (directory == NULL) {
		printf("%d \t N \t %s \t DELETE operation failed - this file cannot be found or accessed\n", linenumber, filename);
		return -1;
	}
	
	char *filename_cpy = strdup(filename);
	char *filename_ret = strdup(filename);
	char *prev_directory_path = find_last_path(filename_cpy);
	free(filename_cpy);
	
	if (strlen(prev_directory_path) < 1) {
		printf("%d \t N \t %s \t DELETE operation failed - no write privileges on top-level directories\n", linenumber, filename);
		free(filename_ret);
		free(prev_directory_path);
		return -1;
	}
	
	struct ll_component *prev_directory = run_write(fs, user, group, prev_directory_path, 1); // previous directory path
	if (prev_directory == NULL) {
		printf("%d \t N \t %s \t DELETE operation failed - write permissions are not permitted\n", linenumber, filename);
		free(filename_ret);
		free(prev_directory_path);
		return -1;
	}
	
	free(prev_directory_path);
	
	if ((&directory->next)->head == 0) { // if the component we want to delete does not have a subdirectory, remove it.
		struct Node *removed = remove_component(&prev_directory->next, directory);
		if (removed == NULL)
			die("Bad.");
		else
			free(removed);
	}
	else { // we can't delete a component if it has a child component.
		printf("%d \t X \t %s \t DELETE operation failed - this component has child components\n", linenumber, filename);
		free(filename_ret);
		return -1;
	}
	
	printf("%d \t Y \t %s \t DELETE operation was successful\n", linenumber, filename);
	
	free(filename_ret);
	return 0;
}

// check that the file doesn't exist by READing through the entire path. then create it.
static int run_create(struct List *fs, char *user, char *group, char *filename) {
	
	struct ll_component *directory = run_read(fs, user, group, filename, 0);
	if (directory != NULL) {
		printf("%d \t X \t %s \t CREATE operation failed - this file already exists\n", linenumber, filename);
		return -1;
	}
	
	char *component;
	char *filename_cpy = strdup(filename);
	char *filename_ret = strdup(filename);
	char *prev_directory_path = find_last_path(filename_cpy);
	
	free(filename_cpy);
	
	if (strlen(prev_directory_path) < 1) {
		printf("%d \t N \t %s \t CREATE operation failed - no write privileges on top-level directories\n", linenumber, filename);
		free(prev_directory_path);
		free(filename_ret);
		return -1;
	}
	
	directory = run_write(fs, user, group, prev_directory_path, 1);
	
	if (directory == NULL) { // can't read the previous component
		printf("%d \t N \t %s \t CREATE operation failed because READ/WRITE operation was unsuccessful\n", linenumber, filename);
		free(prev_directory_path);
		free(filename_ret);
		return -1;
	}
	
	// get the filename we want to create
	char *token = strtok(filename, "/");
	while (token != NULL) {
		component = strdup(token);
		token = strtok(NULL, "/");
		if (token != NULL) {
			free(component);
		}
	}
	
	struct ll_component *new = init_component(&directory->next, component);
	copy_acl(directory, new);
	
	printf("%d \t Y \t %s \t CREATE operation was successful\n", linenumber, filename_ret);
	free(filename_ret);
	free(prev_directory_path);
	free(component);
	return 0;
}

/*
0 - OK
-1 - Permissions error
1 - Invalid ACL entries
*/
static int run_acl(struct List *fs, char *user, char *group, char *filename) {
	struct ll_component *last_directory = run_write(fs, user, group, filename, 1);
	int validity = 0;
	
	if (last_directory == NULL) {
		validity = -1;
	}
	
	char *acl_input = (char *) calloc(MAXBUFFER, sizeof(char));
	struct List new_acl;
	int counter = 0;
	int len = 0;
	int ch;
	
	initList(&new_acl);
	
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch != '\n') {
			acl_input[len++] = ch;
		}
		else {
			acl_input[len] = '\0';
			
			if (strcmp(acl_input, ".") == 0)
				break;

			if (validity == 0) {				
				if (strlen(acl_input) > MAXBUFFER) {
					validity = 1;
					len = 0;
					continue;
				}
			
				char *user = (char *) calloc(strlen(acl_input), sizeof(char));
				char *group = (char *) calloc(strlen(acl_input), sizeof(char));
				char *perm = (char *) calloc(strlen(acl_input), sizeof(char));
			
				sscanf(acl_input, "%[^'.'].%s %s", user, group, perm);
			
				if (validate_string(user) != 0 || validate_string(group) != 0 || strlen(perm) == 0) {
			    	validity = 1;
					free(user);
					free(group);
					free(perm);
					len = 0;
			    	continue;
			    	
				}
			
				if (strcmp(perm, "rw") != 0 && strcmp(perm, "r") != 0 && strcmp(perm, "w") != 0 && strcmp(perm, "-") != 0) {
			    	validity = 1;
					free(user);
					free(group);
					free(perm);
					len = 0;
					continue;
				}
		
				struct acl *entry = (struct acl *) malloc(sizeof(struct acl));
				entry->user = user;
				entry->group = group;
				entry->priv = perm;
				addTail(&new_acl, entry);
			
				++counter;
			}
			len = 0;
		}
	}
	
	free(acl_input);
	
	if (validity != 0) {
		destroy_acl_entries(&new_acl);
	}
	else if (counter > 0) {
		destroy_acl_entries(&last_directory->acl);
		last_directory->acl = new_acl;
	}
		
	
	if (validity == 0) {
		if (counter == 0)
			printf("%d \t X \t %s \t ACL operation failed - null ACL entered\n", linenumber, filename);
		else
			printf("%d \t Y \t %s \t ACL operation successful\n", linenumber, filename);
	}
	
	if (validity == -1) {
		printf("%d \t N \t %s \t ACL operation failed because READ operation was unsuccessful\n", linenumber, filename);
	}
	
	if (validity == 1) {
		printf("%d \t X \t %s \t one or more of your ACL inputs was invalid; nothing done\n", linenumber, filename);
	}
	
	return validity;
}

static int process_ops_command(struct List *fs, char *input) {
	int return_value = -1;
	
	char *command = (char *) calloc(strlen(input) + 1, sizeof(char));
	char *user = (char *) calloc(strlen(input) + 1, sizeof(char));
	char *group = (char *) calloc(strlen(input) + 1, sizeof(char));
	char *filename = (char *) calloc(strlen(input) + 1, sizeof(char));
	
	sscanf(input, "%s %[^'.'].%s %s", command, user, group, filename); // parse the line
	
	if (validate_ops_input(command, user, group, filename) < 0)
		goto exit;
	
	if (strcmp(command, "READ") == 0)
		run_read(fs, user, group, filename, -1);
	else if (strcmp(command, "WRITE") == 0)
		run_write(fs, user, group, filename, 0);
	else if (strcmp(command, "ACL") == 0) 
		return_value = run_acl(fs, user, group, filename);
	else if (strcmp(command, "CREATE") == 0)
		return_value = run_create(fs, user, group, filename);
	else if (strcmp(command, "DELETE") == 0)
		return_value = run_delete(fs, user, group, filename);
	
exit:
	free(command);
	free(user);
	free(group);
	free(filename);
	return return_value;
}

static int process_user_command(struct List *fs, char *input) {
	int return_value = -1;
	char *user = (char *) calloc(strlen(input) + 1, sizeof(char));
	char *group = (char *) calloc(strlen(input) + 1, sizeof(char));
	char *filename = (char *) calloc(strlen(input) + 1, sizeof(char));
	struct ll_component *directory;
	
	sscanf(input, "%[^'.'].%s %s", user, group, filename); // parse the line
	
	int validated = validate_ud_input(fs, user, group, filename);
	if (validated < 0)
		goto exit;
	else if (validated == 0) {
		init_acl(verify, user, group, "rw");
		return_value = 0;
		goto exit;
	}
	
	char *component_val = strdup(filename);
	char *validate = strtok(component_val, "/");
	int token_count = 0;
	
	while (validate != NULL) {
		token_count++;
		if (strlen(validate) > 16) {
			fprintf(stdout, "%d \t X \t filename component input is too long (limit 16)\n", linenumber);
			free(component_val);
			goto exit;
		}
		else if (validate_component(validate) != 0) {
			fprintf(stdout, "%d \t X \t filename component contains invalid characters\n", linenumber);
			free(component_val);
			goto exit;
		}
		
		validate = strtok(NULL, "/");
	}
	
	char *token = strtok(filename, "/");
	
	if ((directory = find_component(fs, token)) == NULL)
		directory = init_component(fs, token);
	
	if (token_count == 1) {
		if ((&directory->next)->head == 0) {
			init_acl(directory, user, group, "rw");
		}
		else {
			printf("%d \t X \t cannot give read-write to a parent directory\n", linenumber);
			goto exit;
		}
	}
	
	// initial component has either been found or created at this point.
	token = strtok(NULL, "/");
	struct ll_component *subdirectory;
	while (token != NULL) {
		if ((subdirectory = find_component(&directory->next, token)) == NULL) {
			subdirectory = init_component(&directory->next, token);
			
			if((token = strtok(NULL, "/")) == NULL) {
				if ((&subdirectory->next)->head == 0) {
					init_acl(subdirectory, user, group, "rw");
				}
				else {
					printf("%d \t X \t cannot give read-write to a parent directory\n", linenumber);
					goto exit;
				}
			}
		}
		else {
			if ((token = strtok(NULL, "/")) == NULL) {
				if ((&subdirectory->next)->head == 0) {
					init_acl(subdirectory, user, group, "rw");
				}
				else {		
					printf("%d \t X \t cannot give read-write to a parent directory\n", linenumber);
					goto exit;
				}
			}
		}
		directory = subdirectory;
	}

	free(component_val);
	return_value = 0;
	
exit:
	free(user);
	free(group);
	free(filename);	
	return return_value;
}

int main() {
    struct List file_system;
    initList(&file_system);
	
	char *inputLine = (char *) malloc(sizeof(char) * MAXBUFFER + 1);
	int ch, len = 0;
	
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch != '\n')
			inputLine[len++] = ch;
		else {
			linenumber++;
			inputLine[len] = '\0';
			
			if (strlen(inputLine) > MAXBUFFER) {
			    printf("%d \t X \t now you're just TRYING to break my program...(input size: %d)\n", linenumber, (int) strlen(inputLine));
				continue;
			}
			
			if (strcmp(inputLine, ".") == 0)
				break;
			
			if(process_user_command(&file_system, inputLine) != -1)
			    printf("%d \t Y \t line was added successfully\n", linenumber);
			
			len = 0;
		}
	}
	
	traverse_fs(&file_system, &addWorldRead);
	linenumber = 0;
	len = 0;
    init_acl(init_component(&file_system, "tmp"),"*", "*", "rw");
	printf("\nEntering Operations...\n\n");
	
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch != '\n')
			inputLine[len++] = ch;
		else {
			linenumber++;
			inputLine[len] = '\0';
			
			if (strlen(inputLine) > MAXBUFFER) {
			    printf("%d \t X \t now you're just TRYING to break my program...(input size: %d)\n", linenumber, (int) strlen(inputLine));
				continue;
			}
			
			process_ops_command(&file_system, inputLine);
			
			len = 0;
		}
	}
	
	// process last line
	if (len > 0) {			
		linenumber++;
		inputLine[len] = '\0';
		
		if (strlen(inputLine) > MAXBUFFER) {
			printf("%d \t X \t now you're just TRYING to break my program...(input size: %d)\n", linenumber, (int) strlen(inputLine));
		}
		
		process_ops_command(&file_system, inputLine);
		
		len = 0;
	}
	
//	printf("\n");
//	traverse_fs(&file_system, &printComponent);
	
	destroy_file_system(&file_system);
	free(inputLine);
    return 0; 
}
