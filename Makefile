OBJ = mylist.o struct.o test.o

build:	acl_simulation

acl_simulation: $(OBJ)
	gcc -Wall -o $@ $(OBJ)

test: build
	cat data/simple_read_write.log | ./acl_simulation
	@echo "------------"
	cat data/simple_acl_create_delete.log | ./acl_simulation
	@echo "------------"
	cat data/master_test.log | ./acl_simulation	
	
exec: build
	cat $(ARG) | ./acl_simulation

clean:
	rm -f *.o a.out *.core acl_simulation
